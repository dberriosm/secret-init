echo $SERVICE_ACCOUNT_VALUE > SERVICE_ACCOUNT_FILE
echo "Configurando cuenta"

if gcloud config set account $SERVICE_ACCOUNT_EMAIL; then
    if gcloud auth   activate-service-account --key-file=SERVICE_ACCOUNT_FILE; then
        echo "Seleccionando proyecto"
        if gcloud config set project $PROJECT_NAME_ID; then
            echo "Bajando archivos"
            gcloud secrets list | tail -n +2 | awk '{print $1}' > secretname
            #!/bin/bash
            while IFS=, read col1
                do
                echo "Creando secreto fichero para secreto" ${col1}
                gcloud secrets versions access latest --secret=${col1} > /secrets/${col1}
            done < secretname
        else
            echo " ** Error al seleccionar proyecto **"
            exit
        fi    
    else
        echo " ** Error al activar cuenta **"
        exit
    fi
else
    echo " ** Error al configurar cuenta **" 
    exit   
fi    
